extern crate tmux_interface;
extern crate serde;
extern crate serde_json;
extern crate dirs;
extern crate exitcode;

#[macro_use]
extern crate serde_derive;

mod api;
use api::rmuxconf::RmuxConf;

use std::path::{PathBuf};
use std::process;

use tmux_interface::{TmuxInterface};
use clap::App;

/// Finds the home directory or errors in the process
fn find_home_dir() -> String {
    // this is bad because it panics when it cant find the file
    // open the file and convert it to the rmux_conf
    let home_dir: PathBuf = match dirs::home_dir() {
        Some(p) => p,
        None => {
            eprintln!("could not find the path of the home_dir.");
            process::exit(exitcode::OSFILE);
        }
    };

    return match home_dir.into_os_string().into_string() {
        Ok(string) => string,
        Err(_) => {
            eprintln!("Failed to read the path of the home_dir.");
            process::exit(exitcode::OSFILE);
        }
    };
}

fn main() {
    // conf path defaults to ~/.config/tmux_setup.json unless specified with -conf path
    let matches = App::new("rmux")
        .version("0.1-beta")
        .about("rust wrapper for tmux")
        .arg("-c, --config=[File] 'Sets a custom config file.'")
        .get_matches();


    let full_path = match matches.value_of("config") {
        // It appears the conf arg is returning none
        Some(conf) => String::from(conf),
        None => {
            let home_dir_path = find_home_dir();
            home_dir_path + "/.config/rmux_setup.json"
        }
    };

    let mut tmux = TmuxInterface::new();

    let rmux_conf = RmuxConf::new(&full_path);

    rmux_conf.generate_tmux_env(&mut tmux);
    rmux_conf.find_session_to_attach_to();
}
