use std::fs::File;
use std::io::Read;
use std::process;

use tmux_interface::{NewSession, TmuxInterface};

/// Currently deserialization of a config into an RmuxConf object is lazy. It does not validate the
/// basic requreiments of the conf. Undefined behavior could occur if the conf is invalid.
/// NOTE: custom deserialization should happen in a later commit
#[derive(Deserialize, Debug)]
pub struct RmuxConf {
    sessions: Vec<RmuxSession>,
}

impl RmuxConf {
    /// Returns a new RmuxConf object from a file
    pub fn new(conf_path: &str) -> RmuxConf {
        let mut file = match File::open(conf_path) {
            Ok(opened_file) => opened_file,
            Err(_) => {
                eprintln!("Failed to open file {}", conf_path);
                process::exit(exitcode::OSFILE);
            }
        };

        let mut conf_data = String::new();
        match file.read_to_string(&mut conf_data) {
            Ok(_) => {}
            Err(_) => {
                eprintln!("Failed to read file to string {}", conf_path);
                process::exit(exitcode::OSFILE);
            }
        };

        let rmux_conf: RmuxConf = match serde_json::from_str(&conf_data) {
            Ok(deserialized) => deserialized,
            Err(_) => {
                eprintln!("Failed to deserialize json string into an RmuxConf object");
                process::exit(exitcode::CONFIG);
            }
        };

        rmux_conf
    }

    pub fn generate_tmux_env(&self, tmux: &mut TmuxInterface) {
        for session_conf in self.get_sessions() {
            let new_session = NewSession {
                /// do not attach to a session until all of them are created
                detached: Some(true),
                session_name: Some(session_conf.get_session_name()),
                ..Default::default()
            };

            //NOTE: This should probably be bubbled up instead
            tmux.new_session(Some(&new_session)).unwrap();

            match session_conf.get_windows().first() {
                None => (),
                Some(window) => {
                    for pane in window.get_panes() {
                        let curr_split = pane.create_split_window();
                        match tmux.split_window(Some(&curr_split)) {
                            Err(err) => {
                                eprintln!("Unable to to create split: {}", err);
                                process::exit(exitcode::UNAVAILABLE);
                            }
                            _ => (),
                        }
                    }
                    ()
                }
            };

            for window in session_conf.get_windows().iter().skip(1) {
                match tmux.new_window(None) {
                    Err(err) => {
                        eprintln!("Failed to create a new window: {}", err);
                        process::exit(exitcode::UNAVAILABLE);
                    }
                    _ => (),
                }

                for pane in window.get_panes() {
                    let curr_split = pane.create_split_window();
                    match tmux.split_window(Some(&curr_split)) {
                        Err(err) => {
                            eprintln!("Unable to to create split: {}", err);
                            process::exit(exitcode::UNAVAILABLE);
                        }
                        _ => (),
                    }
                }

                match tmux.split_window(None) {
                    Err(_) => panic!(),
                    _ => (),
                }
            }
        }
    }

    pub fn get_sessions(&self) -> &Vec<RmuxSession> {
        &self.sessions
    }

    /// Finds the session with default_attach == true and returns the session name
    pub fn find_session_to_attach_to(&self) -> Option<&str> {
        // returning none basically means dont enter into the session
        return Some("aw heck");
    }
}

#[derive(Deserialize, Debug)]
pub struct RmuxSession {
    session_name: String,
    /// All tmux sessions have one window (indexed 0 by default)
    /// session objects must have at least one TmuxWindow, otherwise it is considered invalid
    tmux_windows: Vec<TmuxWindow>,
    /// Defines this as the session in the config to attach to. Only one session can be attached to at a time.
    default_attach: bool,
}

impl RmuxSession {
    pub fn get_session_name(&self) -> &str {
        &self.session_name
    }

    pub fn get_default_attach(&self) -> bool {
        self.default_attach
    }

    pub fn get_windows(&self) -> &Vec<TmuxWindow> {
        &self.tmux_windows
    }
}

#[derive(Deserialize, Debug)]
pub struct TmuxWindow {
    // panes can be empty vector. Any panes in the array are considered horizantal splits
    panes: Vec<TmuxPane>,
}

impl TmuxWindow {
    pub fn get_panes(&self) -> &Vec<TmuxPane> {
        &self.panes
    }
}

#[derive(Deserialize, Debug)]
pub enum SplitType {
    VERTICAL,
    HORIZANTAL,
    NONE,
}

#[derive(Deserialize, Debug)]
pub struct TmuxPane {
    split_type: SplitType,
    panes: Vec<TmuxPane>,
    send_keys: Vec<String>,
}

impl TmuxPane {
    fn create_split_window(&self) -> tmux_interface::windows_and_panes::SplitWindow {
        // first window is automatically created. just get the internal panes of that.
        tmux_interface::windows_and_panes::SplitWindow {
            before: None,
            detached: None,
            full: None,
            horizontal: match &self.split_type {
                SplitType::VERTICAL => Some(false),
                SplitType::NONE => Some(false),
                SplitType::HORIZANTAL => Some(true),
            },
            stdin_forward: None,
            vertical: match &self.split_type {
                SplitType::VERTICAL => Some(true),
                SplitType::NONE => Some(false),
                SplitType::HORIZANTAL => Some(false),
            },
            print: None,
            cwd: None,
            size: None,
            environment: None,
            target_pane: None,
            shell_command: None,
            format: None,
        }
    }
}
